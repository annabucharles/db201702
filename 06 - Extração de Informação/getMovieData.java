package testehttpok;
import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import java.io.File;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Objects;
import java.sql.SQLException;



public class getMovieData {
	
	OkHttpClient client = new OkHttpClient();

  String run(String url) throws IOException {
    Request request = new Request.Builder()
        .url(url)
        .build();

    try (Response response = client.newCall(request).execute()) {
      return response.body().string();
    }
  }

  public static void main(String[] args) throws IOException {
    getMovieData example = new getMovieData();
    String year, idMovie, movieName, director, genre, idAffinity;
    String awards; //a informação extra
    String duracao; //informacao extra de fonte alternativa 
    Connection con = null;
    PreparedStatement stmt = null;
    
    int auxid = 1;
    String id = "m" + auxid; //m de movie
    
    /*FileWriter movie = new FileWriter("movie.xml");
    PrintWriter gravar_filmes = new PrintWriter(movie);
    //gravar_filmes.printf("<Movies>");*/

    try {
    	
    	Class.forName("org.postgresql.Driver");
    con = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls", "353330");


        con.setAutoCommit(false);
        System.out.println("Opened database successfully");

    	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
	Document movie = docBuilder.newDocument();
	Element rootElement = movie.createElement("Movies");
	movie.appendChild(rootElement);
		
    	File fXmlFile = new File("movie.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);

	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

	NodeList nList = doc.getElementsByTagName("LikesMovie");

	System.out.println("----------------------------");
	
	for (int temp = 0; temp < nList.getLength(); temp++) {

		Node nNode = nList.item(temp);
		

		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;
			idMovie = eElement.getAttribute("movieUri");
		  
		    
			idMovie = idMovie.split("title/")[1].split("/")[0]; //pega apenas o ID pra colocar na api
		    String s = new String ("http://www.omdbapi.com/?i=" + idMovie + "&plot=full&apikey=PlsBanMe&r=xml"); //string que "monta" o url a ser requisitado para a API
		    
		    String response = example.run(s);
		    /* aqui ele separa os atributos e coloca eles em variáveis que podem ser manipuladas */ 
		    awards = response.split("awards"+"=\"")[1].split("\"")[0]; //pega o que tem dentro do awards=""
		    year = response.split("year"+"=\"")[1].split("\"")[0]; //pega o que tem dentro do awards=""
		    movieName = response.split("movie title"+"=\"")[1].split("\"")[0];
		    director = response.split("director"+"=\"")[1].split("\"")[0];
		    genre = response.split("genre"+"=\"")[1].split("\"")[0];
		    System.out.println("Título : " + movieName);
		    System.out.println("Ano: " + year);
		    System.out.println("Diretor: " + director);
		    System.out.println("Genero: " + genre);
		    System.out.println("Awards:" + awards);	
		    /*Pega a informação de outra fonte alternativa */
		    s = "https://filmaffinity-api.mybluemix.net/api/film/byTitle?title=" + movieName;
		    response = example.run(s);
		    if (Objects.equals("[]", response)) {
		    		duracao = "Nao disponivel";
		    }
		    else {
		    		System.out.println(response);
		    		idAffinity = response.split("url\":\"", 2)[1].split("\"")[0];
		    		s = "https://filmaffinity-api.mybluemix.net/api/film/byId?id=" + idAffinity;
		    		response = example.run(s);
		    		try {duracao = response.split("\"Duración\":\"")[1].split(".\",")[0];
		    		}
		    		catch (ArrayIndexOutOfBoundsException e){ duracao = "Nao disponivel";}
		    }
		    
		    
		    System.out.println("Duração: " + duracao);

			
			auxid++;
			id = "m" + auxid;
			/* escrever  no BANCO DE DADOS aqui*/
			stmt = con.prepareStatement("insert into filme (nome,id,dt_lan,awards,diretor,genero,duracao) values (?,?,?,?,?,?,?)");
			// preenche os valores
			stmt.setString(1, movieName);
			stmt.setString(2, id);
			stmt.setString(3, year);
			stmt.setString(4, awards);
			stmt.setString(5, director);
			stmt.setString(6, genre);
			stmt.setString(7, duracao);
			
			con.commit();
			stmt.executeUpdate();
			
			/*stmt.executeUpdate("INSERT INTO filme " + 
		                "VALUES ('" + movieName + "','" + id + "','" + year + "','" + awards + "','" + director + "','" + genre + "');"); 		     
			*/
			/*Escreve num arquivo de saída*/ 

			Element movieInfo = movie.createElement("Movie");
			rootElement.appendChild(movieInfo);

			// set attributes
			Attr attrtitle = movie.createAttribute("titulo");
			attrtitle.setValue(movieName);
			movieInfo.setAttributeNode(attrtitle);
			Attr attrYear = movie.createAttribute("ano");
			attrYear.setValue(year);
			movieInfo.setAttributeNode(attrYear);
			Attr attrDirector = movie.createAttribute("diretor");
			attrDirector.setValue(director);
			movieInfo.setAttributeNode(attrDirector);
			Attr attrGenre = movie.createAttribute("genero");
			attrGenre.setValue(genre);
			movieInfo.setAttributeNode(attrGenre);
			Attr attrAwards = movie.createAttribute("awards");
			attrAwards.setValue(awards);
			movieInfo.setAttributeNode(attrAwards);
			Attr attrDuracao = movie.createAttribute("duracao");
			attrDuracao.setValue(duracao);
			movieInfo.setAttributeNode(attrDuracao);
		    
		}
		
	}
	stmt.close();
	con.close(); 
	TransformerFactory transformerFactory = TransformerFactory.newInstance();
	Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(movie);
	StreamResult result = new StreamResult(new File("/Users/ana/eclipse-workspace/testehttpok/out/movie.xml"));
	transformer.transform(source, result);

	System.out.println("File saved!");

	
    } catch (Exception e) {
	e.printStackTrace();
    }
  
  }
}
