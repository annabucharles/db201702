package getMusicData;
import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import java.io.File;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Objects;
import java.sql.SQLException;



public class getMusicData {
	
	OkHttpClient client = new OkHttpClient();

  String run(String url) throws IOException {
    Request request = new Request.Builder()
        .url(url)
        .build();

    try (Response response = client.newCall(request).execute()) {
      return response.body().string();
    }
  }

  public static void main(String[] args) throws IOException {
    getMusicData example = new getMusicData();
    String nome, origem, genre, idmusic;
    String descricao; //a informação extra
    char c;
    Connection con = null;
    PreparedStatement stmt = null;
    
    int id = 0;
    
  
    try {
    	
    	Class.forName("org.postgresql.Driver");
    con = DriverManager.getConnection("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls", "353330");


        con.setAutoCommit(false);
        System.out.println("Opened database successfully");

    	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
	Document music = docBuilder.newDocument();
	Element rootElement = music.createElement("Musics");
	music.appendChild(rootElement);
		
    	File fXmlFile = new File("music.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);

	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

	NodeList nList = doc.getElementsByTagName("LikesMusic");

	
	for (int temp = 0; temp < nList.getLength(); temp++) {


		Node nNode = nList.item(temp);


		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;
			idmusic = eElement.getAttribute("bandUri");
			
		    idmusic = idmusic.substring(idmusic.lastIndexOf("/") + 1); //pega apenas o ID pra colocar na api
		    
		    //https://en.wikipedia.org/w/api.php?action=opensearch&search=bee&limit=1&format=json

		    String s = new String ("https://en.wikipedia.org/w/api.php?action=opensearch&search=" + idmusic + "&limit=1&format=xml"); //string que "monta" o url a ser requisitado para a API
		    
		    String response = example.run(s);
			
		    //pega descricao
		    descricao = response.split("<Description xml:space=" + "\"" + "preserve" + "\">")[1].split("</Description>")[0];


		    for (int count = 0; count < idmusic.length(); count++){ //deixa  o nome do artista com espaços pois o search na api nao funciona com underline
    			c = idmusic.charAt(count);
		    		if (idmusic.charAt(count) == '_'){
		    			char[] im_aux = idmusic.toCharArray();
		    			im_aux[count] = ' ';
		    			idmusic = String.valueOf(im_aux);
		    		}
    			}
	    		nome = idmusic; //define nome aqui ja!!
	    		System.out.println("Banda : " + nome);
	    		s = "http://api.musicgraph.com/api/v2/artist/search?api_key=c8303e90962e3a5ebd5a1f260a69b138&name="+ idmusic;
	    		
	    		response = example.run(s);
	    		
	    		
	    		
	    		//verifica se existe na fonte que estamos vendo
	    		if (Objects.equals("{\"status\": {\"api\": \"v2\", \"message\": \"Success\", \"code\": 0}, \"pagination\": {\"count\": 0, \"offset\": 1}, \"data\": []}", response) == true || id == 25) {
	   
	    			origem = "nao disponivel";
	    			genre = "nao disponivel";
	    			
	    		}
	    		
	    		else { 
	    			try{idmusic = response.split("\"id\": \"", 2)[1].split("\",")[0];}
	    			catch (ArrayIndexOutOfBoundsException e){ idmusic = "Nao disponivel"; continue;}
		    		s = "http://api.musicgraph.com/api/v2/artist/"+idmusic+"?api_key=c8303e90962e3a5ebd5a1f260a69b138";
		    		
		    		response = example.run(s);
		    		
			    /* aqui ele separa os atributos e coloca eles em variáveis que podem ser manipuladas */ 
		    		
		    		try {
		    			
		    		origem = response.split("\"country_of_origin\": \"")[1].split("\",")[0]; //origem
		    		 genre = response.split("\"main_genre\": \"")[1].split("\",")[0]; //generos musicais
		    		}
		    		catch (ArrayIndexOutOfBoundsException e){ origem = "Nao disponivel"; 
		    		genre = "nao disponivel";}
			    
			   
	    		}
	    		
		    System.out.println("Breve descricao:" + descricao);
		    System.out.println("Banda : " + nome);
		    System.out.println("Origem: " + origem);
		    System.out.println("Generos: " + genre);
			System.out.println("ID2 : " + id);


			id++;
			new Thread().sleep(40500); //o sistema se sobrecarrega (?)

			/* escrever  no BANCO DE DADOS aqui*/
			stmt = con.prepareStatement("insert into artista_musical (id,nome_artista,pais,genero,brevedesc) values (?,?,?,?,?)");
			// preenche os valores
			stmt.setInt(1, id);
			stmt.setString(2, nome);
			stmt.setString(3, origem);
			stmt.setString(4, genre);
			stmt.setString(5, descricao);
			
			con.commit();
			stmt.executeUpdate();
			

			/*Escreve num arquivo de saída*/ 

			Element musicInfo = music.createElement("music");
			rootElement.appendChild(musicInfo);

			// set attributes
			Attr attrnome = music.createAttribute("artista");
			attrnome.setValue(nome);
			musicInfo.setAttributeNode(attrnome); //define o nome do artista
			
			
			Attr attrOrigem = music.createAttribute("origem");
			attrOrigem.setValue(origem);
			musicInfo.setAttributeNode(attrOrigem); //define origem do artista
			
			
			Attr attrGenero = music.createAttribute("genero");
			attrGenero.setValue(genre);
			musicInfo.setAttributeNode(attrGenero); //define genero do artista
			
			
			Attr attrbreveDescricao = music.createAttribute("breveDescricao");
			attrbreveDescricao.setValue(descricao);
			musicInfo.setAttributeNode(attrbreveDescricao); //seta breve descrição do artista
			
			
			
		}
		
	}
	stmt.close();
	con.close(); 
	TransformerFactory transformerFactory = TransformerFactory.newInstance();
	Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(music);
	StreamResult result = new StreamResult(new File("/Users/ana/eclipse-workspace/getMusicData/out/music.xml"));
	transformer.transform(source, result);

	System.out.println("File saved!");

	
    } catch (Exception e) {
	e.printStackTrace();
    }
  
  }
}
