package com.chart;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class PieTest {

   public static void main(String[] args) throws IOException {
      String fileName = args.length > 0 ? args[0] : "piechart.png";
      JFreeChart pieChart = createChart(createDataset());
      File chartFile = new File(fileName);
      ChartUtilities.saveChartAsPNG(chartFile, pieChart, 300, 200);
   }

   private static PieDataset createDataset() {
      DefaultPieDataset dataset = new DefaultPieDataset();

    dataset.setValue("1 pessoas curtiram",new Double(4));
    dataset.setValue("2 pessoas curtiram",new Double(8));
    dataset.setValue("1 pessoas curtiram",new Double(10));
    dataset.setValue("8 pessoas curtiram",new Double(14));
    dataset.setValue("6 pessoas curtiram",new Double(16));
    dataset.setValue("1 pessoas curtiram",new Double(18));
    dataset.setValue("1 pessoas curtiram",new Double(20));
    dataset.setValue("1 pessoas curtiram",new Double(22));
    dataset.setValue("1 pessoas curtiram",new Double(36));

      return dataset;
  }

   private static JFreeChart createChart(PieDataset dataset) {

      JFreeChart chart = ChartFactory.createPieChart(
         "número de pessoas que curtiram exatamente x filmes.",
         dataset,
         false,
         false,
         false);

      chart.setBorderVisible(true);
      chart.setBackgroundPaint(Color.WHITE);

      TextTitle title = chart.getTitle();
      title.setFont(new Font("SansSerif", Font.BOLD, 12));
      title.setPaint(Color.GRAY);

      PiePlot plot = (PiePlot)chart.getPlot();
      plot.setNoDataMessage("No data available");
      plot.setIgnoreZeroValues(true);
      plot.setCircular(false);
      plot.setOutlinePaint(null);
      plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 8));
      plot.setLabelGap(0.02);
      plot.setLabelOutlinePaint(null);
      plot.setLabelShadowPaint(null);
      plot.setLabelBackgroundPaint(Color.WHITE);
      plot.setBackgroundPaint(Color.WHITE);

      return chart;
  }

}