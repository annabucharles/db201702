    import java.io.File;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.DocumentBuilder;
    import org.w3c.dom.Document;
    import org.w3c.dom.NodeList;


    import org.w3c.dom.Node;
    import org.w3c.dom.Element;
    import java.sql.*;
    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.Statement;
    import java.sql.SQLException;
    import java.util.Objects;
    import java.util.Scanner;
    import java.util.logging.Level;
    import java.util.logging.Logger;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;

    class Artistas {
        double media_rating;
        int repeticoes;
        String nome_artista;
        int ranting;
        String genero;
    }

    class Filmes {
        double media_rating;
        int repeticoes;
        String nome_filme;
    }

    public class consultas {

        public static void main(String[] args) {

          try {
            Class.forName("org.postgresql.Driver");
             System.out.print("***** 1. Qual é a média e desvio padrão dos ratings para artistas musicais e filmes? \n");
             pergunta1();

             System.out.println("***** 2. Quais são os artistas e filmes com o maior rating médio curtidos por pelo menos duas pessoas? Ordenados por rating médio.");
             pergunta2e3();

            //  System.out.println("***** 4. View ConheceNormalizada");
            //  pergunta4();

             System.out.println("***** 5. Quais são os conhecidos (duas pessoas ligadas na view ConheceNormalizada) que compartilham o maior numero de filmes curtidos?");
             pergunta5();

             System.out.println("***** 6. Qual o número de conhecidos dos conhecidos (usando ConheceNormalizada) para cada integrante do grupo??");
             pergunta6();

             System.out.println("***** 9. Defina duas outras informações (como as anteriores) que seriam úteis para compreender melhor a rede. Agregue estas informações à sua aplicação.");
             pergunta9();
             pergunta9_2();

          }catch (Exception e) {
            e.printStackTrace();
          }

        }

        public static Connection connection_my(String url,String login,String senha) throws SQLException {
                Connection con = null;
                con = (Connection) DriverManager.getConnection(url,login,senha);
              if(con == null){
                System.out.print("Banco não conectado");
              }
            return con;
        }

    //Qual é a média e desvio padrão dos ratings para artistas musicais e filmes?
        public static void pergunta1(){
          Statement stmt = null;
          Statement stmt2 = null;
          Statement stmt3 = null;
          Statement stmt4 = null;
          try {
            Connection con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
            con.setAutoCommit(false);

            stmt = con.createStatement();
            stmt2 = con.createStatement();
            stmt3 = con.createStatement();
            stmt4 = con.createStatement();
            ResultSet media_musica = stmt.executeQuery("SELECT  AVG(rating) FROM like_music;" );
            ResultSet media_filme = stmt2.executeQuery("SELECT  AVG(rating) FROM like_movie;" );
            ResultSet dp_filme = stmt3.executeQuery("SELECT  stddev(rating) FROM like_movie;" );
            ResultSet dp_musica = stmt4.executeQuery("SELECT  stddev(rating) FROM like_music;" );


            while(media_musica.next() && media_filme.next() && dp_musica.next() && dp_filme.next()){
              float  mediaM = media_musica.getFloat("avg");

               System.out.print("Media ratings da musica = " + mediaM + "\n");

               float  mediaF = media_filme.getFloat("avg");

                System.out.print("Media ratings do filme = " + mediaF + "\n");

                float  dpF = dp_filme.getFloat("stddev");

                 System.out.print("Desvio padrao do filme = " + dpF + "\n");

                 float  dpM= dp_musica.getFloat("stddev");

                  System.out.print("Desvio padrao musica = " + dpM + "\n");

            }

            stmt.close();
            stmt2.close();
            stmt3.close();
            stmt4.close();
            con.commit();
            con.close();

          }catch (Exception e) {
            e.printStackTrace();
          }
        }


        public static void pergunta2e3() {
                int i = 0, total_tuplas = 186, total_filmes = 348, indice_filme = 0, indice_artista = 0, j;
                boolean repetiu = false;
                    Artistas artista[];
                    artista = new Artistas [total_tuplas];
                    Filmes filme[];
                    filme = new Filmes[total_filmes];


                Statement stmt = null;
                Statement stmt2 = null;
                try{
                  Connection con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
                  con.setAutoCommit(false);

                  stmt = con.createStatement();
                  stmt2 = con.createStatement();

                  ResultSet music = stmt.executeQuery( "SELECT * FROM like_music;" );
                  ResultSet movie = stmt2.executeQuery( "SELECT * FROM like_movie;");

                  while ( movie.next() ) {

                      String uri_filme = movie.getString("uri");
                      int rating_filme = movie.getInt("rating");


                    if (i == 0) {
                        filme[indice_filme] = new Filmes();
                        filme[indice_filme].nome_filme = uri_filme;
                        filme[indice_filme].media_rating = rating_filme;
                        indice_filme++;
                    }

                    if (i > 0) { //verifica outros artistas
                        for (j = 0; j < indice_filme; j++) {
                            if (Objects.equals(filme[j].nome_filme,uri_filme)){ //ja existe um filme igual

                                double x, total_pessoas;
                                total_pessoas = filme[j].repeticoes + 1;
                                x = filme[j].media_rating * total_pessoas;
                                filme[j].media_rating = (x+rating_filme)/(total_pessoas+1); //calcula media rating de cada artista
                                filme[j].repeticoes++;
                                repetiu = true;
                                break;
                            }

                        }
                        //se não for repetido

                        if (!repetiu) {
                            filme[indice_filme] = new Filmes();
                            filme[indice_filme].nome_filme = uri_filme;
                            filme[indice_filme].media_rating = rating_filme;
                            filme[indice_filme].repeticoes = 0;
                            indice_filme++;
                        }
                        repetiu = false;

                    }

                    i++;
                }


                 while ( music.next() ) {

                       String uri = music.getString("uri");
                       int rating = music.getInt("rating");


                        if (i == 0) {
                            artista[indice_artista] = new Artistas();
                            artista[indice_artista].nome_artista = uri;
                            artista[indice_artista].media_rating = rating;
                            indice_artista++;
                        }

                        if (i > 0) { //verifica outros artistas
                            for (j = 0; j < indice_artista; j++) {
                                if (Objects.equals(artista[j].nome_artista,uri)){ //ja existe um filme igual

                                    double x, total_pessoas;
                                    total_pessoas = artista[j].repeticoes + 1;
                                    x = artista[j].media_rating * total_pessoas;
                                    artista[j].media_rating = (x+rating)/(total_pessoas+1); //calcula media rating de cada artista
                                    artista[j].repeticoes++;
                                    repetiu = true;
                                    break;
                                }

                            }
                            //se não for repetido

                            if (!repetiu) {
                                artista[indice_artista] = new Artistas();
                                artista[indice_artista].nome_artista = uri;
                                artista[indice_artista].media_rating = rating;
                                artista[indice_artista].repeticoes = 0;
                                indice_artista++;
                            }
                            repetiu = false;

                        }

                        i++;
                 }



                 int maisde2_artistas = 0, maisde2_filmes = 0;
                 Artistas top_artistas[];
                 Filmes top_filmes[];
                 top_artistas = new Artistas [total_tuplas];
                 top_filmes = new Filmes[total_filmes];

                 for (i = 0; i < indice_artista; i++) {
                        if (artista[i].repeticoes >= 1) { //pega todos os artistas
                            top_artistas[maisde2_artistas] = new Artistas();
                            top_artistas[maisde2_artistas].repeticoes = artista[i].repeticoes;
                            top_artistas[maisde2_artistas].nome_artista = artista[i].nome_artista;
                            top_artistas[maisde2_artistas].media_rating = artista[i].media_rating;
                            maisde2_artistas++;
                        }
                 }
                 for (i = 0; i < indice_filme; i++) {
                    if (filme[i].repeticoes >= 1) { //pega todos os artistas
                        top_filmes[maisde2_filmes] = new Filmes();
                        top_filmes[maisde2_filmes].repeticoes = filme[i].repeticoes;
                        top_filmes[maisde2_filmes].nome_filme = filme[i].nome_filme;
                        top_filmes[maisde2_filmes].media_rating = filme[i].media_rating;
                        maisde2_filmes++;
                    }
              }
                 Artistas aux;
                 aux = new Artistas();
                 Filmes aux_filme;
                 aux_filme = new Filmes();

                 for (i = 1; i < maisde2_artistas; i++) {
                         for(j = 0; j<i; j++){
                            if(top_artistas[i].media_rating > top_artistas[j].media_rating){
                                aux = top_artistas[i];
                                top_artistas[i] = top_artistas[j];
                                top_artistas[j] = aux;
                            }
                         }
                 }
                 for (i = 1; i < maisde2_filmes; i++) {
                     for(j = 0; j<i; j++){
                        if(top_filmes[i].media_rating > top_filmes[j].media_rating){
                            aux_filme = top_filmes[i];
                            top_filmes[i] = top_filmes[j];
                            top_filmes[j] = aux_filme;
                        }
                        }
                 }

                 System.out.println("\n### Artistas com maior rating medio curtido por pelo menos duas pessoas: ");
                 for (i = 0; i < 10; i++) {
                        System.out.println ("Artista: " + top_artistas[i].nome_artista + " media  " +  top_artistas[i].media_rating);

                 }

                 System.out.println("\n### Filmes com maior rating medio curtido por pelo menos duas pessoas: ");
                 for (i = 0; i < 30; i++) {

                        System.out.println ("Filme: " + top_filmes[i].nome_filme + " media:  " +  top_filmes[i].media_rating);

                 }
                 System.out.println("Quais são os 10 artistas musicais e filmes mais populares? Ordenados por popularidade.\n");
                 for (i = 1; i < maisde2_filmes; i++) {
                     for(j = 0; j<i; j++){
                        if(top_filmes[i].repeticoes > top_filmes[j].repeticoes){
                            aux_filme = top_filmes[i];
                            top_filmes[i] = top_filmes[j];
                            top_filmes[j] = aux_filme;
                        }
                        }
                 }
                 for (i = 1; i < maisde2_artistas; i++) {
                     for(j = 0; j<i; j++){
                        if(top_artistas[i].repeticoes > top_artistas[j].repeticoes){
                            aux = top_artistas[i];
                            top_artistas[i] = top_artistas[j];
                            top_artistas[j] = aux;
                        }
                        }
                 }
                 for (i = 0; i < 10; i++) {
                    int vezes = top_filmes[i].repeticoes+1;
                    System.out.println ("Filme: " + top_filmes[i].nome_filme + " Curtido por " +  vezes + " pessoas, " + " media  " +  top_filmes[i].media_rating);

                 }

                 for (i = 0; i < 10; i++) {
                    int vezes = top_artistas[i].repeticoes+1;
                    System.out.println ("Artista: " + top_artistas[i].nome_artista + "    Curtido por " +  vezes + "   pessoas, " + "   media  " +  top_artistas[i].media_rating);

              }

                     stmt.close();
                     con.commit();
                     con.close();
                 }catch(Exception e){
                     e.printStackTrace();
                 }

        }

   //
  //   //sÓ EXECUTA ESSA FUNÇÃO PARA CRIAR UMA NOVA VIEW
  //    public static void pergunta4(){
  //     Connection con = null;
  //       try {
  //             con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
  //         String sql = "CREATE VIEW ConheceNormalizada as " +
  //                      "SELECT c.c01,c.co2 " +
  //                      " from conhece c " +
  //                      " UNION " +
  //                      " SELECT c.c01 as co2, c.co2 as c01 " +
  //                      " FROM conhece c;";
  //         PreparedStatement ps = con.prepareStatement(sql);
  //        ps.executeUpdate();
  //         System.out.println("Criou :)))))");
  //         ps.close();
  //         con.close();
  //       }catch(Exception e){
  //         e.printStackTrace();
  //       }
  //  }


       public static void pergunta5(){
         Connection con = null;
           try {
                 con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
             String sql = "WITH mutual_likes AS ( " +
                          " select c01.login as c0, co2.login as c1, COUNT(DISTINCT c01.uri) as count " +
                          " from conhecenormalizada join like_movie c01 on (conhecenormalizada.c01 = c01.login) " +
                          "  join like_movie co2 on (conhecenormalizada.co2 = co2.login) " +
                          "   where c01.login != co2.login " +
                          "      and c01.uri = co2.uri   "   +
                          "     group by c01.login, co2.login) " +
                          "  select * from mutual_likes where mutual_likes.count = ( " +
                          "   select MAX(mutual_likes.count) as n_likes " +
                          "    from mutual_likes);" ;
             PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
              String c0 = rs.getString("c0");
              String c1 = rs.getString("c1");
              int count = rs.getInt("count");
               System.out.println( " Pessoa 1: " + c0 + " Conhecido: " + c1 + " Número de filmes iguais: " +count + "\n");
            }
             ps.close();
             con.close();
           }catch(Exception e){
             e.printStackTrace();
           }
      }

      public static void pergunta6(){
        Statement stmt = null;
        try{
          Connection con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
          con.setAutoCommit(false);
          stmt = con.createStatement();
          String sql = " select CN.c01,CN.co2,n_conhecidos.soma  from ConheceNormalizada CN " +
                         " join (select COUNT (co2) as soma,c01 " +
                        "  from ConheceNormalizada group by c01) as n_conhecidos on CN.co2 = n_conhecidos.c01;" ;
          ResultSet rs = stmt.executeQuery(sql );
          while(rs.next()){
            String p1 = rs.getString("c01");
            String p2 = rs.getString("co2");
            int resultado = rs.getInt("soma");
            System.out.println("Pessoa1: " + p1 + " " +"Pessoa2: " + p2 + " " + "Número de conhecidos dos conhecidos: " + resultado);
          }
          stmt.close();
          con.close();
        }catch (Exception e) {
          e.printStackTrace();
        }
     }

     public static void pergunta9(){
       Statement stmt = null;
       Statement stmt2 = null;
       try{
         Connection con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
         con.setAutoCommit(false);

         stmt = con.createStatement();

         ResultSet rs = stmt.executeQuery("select distinct lm.rating,am.genero from like_music lm natural join artista_musical am where lm.rating = (select max(rating) from like_music);");
         System.out.print("Os gêneros musicais mais populares curtidos pelos usúarios: " + "\n");
         while (rs.next()){
           int ranking = rs.getInt("rating");
           String g_m = rs.getString("genero");

           System.out.print(g_m + "\n ");
         }
         stmt.close();
         con.close();
       } catch (Exception e) {
         e.printStackTrace();
       }
     }

     public static void pergunta9_2(){
       Statement stmt = null;
       Statement stmt2 = null;
       try{
         Connection con = connection_my("jdbc:postgresql://200.134.10.32:5432/1702PowerPuffGirls","1702PowerPuffGirls","353330");
         con.setAutoCommit(false);

         stmt = con.createStatement();

         ResultSet rs = stmt.executeQuery("select distinct lf.rating,f.genero from like_movie lf  join filme f on lf.rating = (select max(rating) from like_movie);");
         System.out.print("Os gêneros de filmes mais populares curtidos pelos usúarios: " + "\n");
         while (rs.next()){
           int ranking = rs.getInt("rating");
           String g_f = rs.getString("genero");

           System.out.print(g_f + "\n ");
         }
         stmt.close();
         con.close();
       } catch (Exception e) {
         e.printStackTrace();
       }
     }
}
