package com.chart;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class PieTest {

   public static void main(String[] args) throws IOException {
      String fileName = args.length > 0 ? args[0] : "piechart.png";
      JFreeChart pieChart = createChart(createDataset());
      File chartFile = new File(fileName);
      ChartUtilities.saveChartAsPNG(chartFile, pieChart, 300, 200);
   }

   private static PieDataset createDataset() {
      DefaultPieDataset dataset = new DefaultPieDataset();

    dataset.setValue("katielensilva", new Double(16));
    dataset.setValue("renatacarvalho", new Double(14));
    dataset.setValue("gabrielfernandes", new Double(8));
    dataset.setValue("otaviobastos", new Double(16));
    dataset.setValue("mateushercules", new Double(36));
    dataset.setValue("gustavolibel", new Double(16));
    dataset.setValue("rodrigosouza", new Double(14));
    dataset.setValue("gabrielsilva", new Double(14));
    dataset.setValue("lucasseixas", new Double(14));
    dataset.setValue("tomasbarreiros", new Double(14));
    dataset.setValue("marcelomarciniak", new Double(14));
    dataset.setValue("marcostholken", new Double(18));
    dataset.setValue("amandalima", new Double(20));
    dataset.setValue("barbaraguerios", new Double(14));
    dataset.setValue("annabucharles", new Double(10));
    dataset.setValue("matheusgoncalves", new Double(14));
    dataset.setValue("lucasmatos", new Double(22));
    dataset.setValue("fabianczajkowski", new Double(14));
    dataset.setValue("matheusgutierrez", new Double(16));
    dataset.setValue("gustavopupo", new Double(8));
    dataset.setValue("igorlatini", new Double(16));
    dataset.setValue("natanreginaldo", new Double(16));

      return dataset;
  }

   private static JFreeChart createChart(PieDataset dataset) {

      JFreeChart chart = ChartFactory.createPieChart(
         "número de pessoas que curtiram exatamente x filmes.",
         dataset,
         false,
         false,
         false);

      chart.setBorderVisible(true);
      chart.setBackgroundPaint(Color.WHITE);

      TextTitle title = chart.getTitle();
      title.setFont(new Font("SansSerif", Font.BOLD, 12));
      title.setPaint(Color.GRAY);

      PiePlot plot = (PiePlot)chart.getPlot();
      plot.setNoDataMessage("No data available");
      plot.setIgnoreZeroValues(true);
      plot.setCircular(false);
      plot.setOutlinePaint(null);
      plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 8));
      plot.setLabelGap(0.02);
      plot.setLabelOutlinePaint(null);
      plot.setLabelShadowPaint(null);
      plot.setLabelBackgroundPaint(Color.WHITE);
      plot.setBackgroundPaint(Color.WHITE);

      return chart;
  }

}