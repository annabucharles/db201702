import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

import java.io.FileWriter;
import java.io.PrintWriter;

public class ReadXMLFile {

    public static void main(String argv[]) {

        int peso = 0;
        float pesoHulk = 0;
        float altura = 0;
        int good = 0;
        int bad = 0;
       
        try {

            File fXmlFile = new File("/home/kati/Dropbox/UTFPR/Introdução a bancos de dados/db201702/01 - Modelos de dados e XML/marvel_simplificado.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("hero");

            System.out.println("----------------------------");


            //Criando o diretório dadosMarvel
            File dadosMarvel = new File("/home/kati/Dropbox/UTFPR/Introdução a bancos de dados/db201702/01 - Modelos de dados e XML");
            dadosMarvel.mkdir();

            //Criando arquivo csv em dados tabulares
            FileWriter herois = new FileWriter("/home/kati/Dropbox/UTFPR/Introdução a bancos de dados/db201702/01 - Modelos de dados e XML/dadosMarvel/herois.csv");
            PrintWriter gravar_herois = new PrintWriter(herois);

            FileWriter herois_good = new FileWriter("/home/kati/Dropbox/UTFPR/Introdução a bancos de dados/db201702/01 - Modelos de dados e XML/dadosMarvel/herois_good.csv");
            PrintWriter gravar_herois_good = new PrintWriter(herois_good);

            FileWriter herois_bad = new FileWriter("/home/kati/Dropbox/UTFPR/Introdução a bancos de dados/db201702/01 - Modelos de dados e XML/dadosMarvel/herois_bad.csv");
            PrintWriter gravar_herois_bad = new PrintWriter(herois_bad);

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    System.out.println("ID : " + eElement.getAttribute("id"));
                    System.out.println("Name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
                    





                    gravar_herois.printf(eElement.getAttribute("id") + ", " + eElement.getElementsByTagName("name").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("popularity").item(0).getTextContent() + ", " + eElement.getElementsByTagName("alignment").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("gender").item(0).getTextContent() + ", " + eElement.getElementsByTagName("height_m").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("weight_kg").item(0).getTextContent() + ", " + eElement.getElementsByTagName("hometown").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("intelligence").item(0).getTextContent() + ", " + eElement.getElementsByTagName("strength").item(0).getTextContent() + ", " + 
                            eElement.getElementsByTagName("durability").item(0).getTextContent() + ", " + eElement.getElementsByTagName("energy_Projection").item(0).getTextContent() + ", " 
                            + eElement.getElementsByTagName("fighting_Skills").item(0).getTextContent() + "\n");

                   p
                    if (eElement.getElementsByTagName("alignment").item(0).getTextContent().equals("Good")) {
                        gravar_herois_good.printf(eElement.getAttribute("id") + ", " + eElement.getElementsByTagName("name").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("popularity").item(0).getTextContent() + ", " + eElement.getElementsByTagName("alignment").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("gender").item(0).getTextContent() + ", " + eElement.getElementsByTagName("height_m").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("weight_kg").item(0).getTextContent() + ", " + eElement.getElementsByTagName("hometown").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("intelligence").item(0).getTextContent() + ", " + eElement.getElementsByTagName("strength").item(0).getTextContent() + ", " + 
                            eElement.getElementsByTagName("durability").item(0).getTextContent() + ", " + eElement.getElementsByTagName("energy_Projection").item(0).getTextContent() + ", " 
                            + eElement.getElementsByTagName("fighting_Skills").item(0).getTextContent() + "\n");

                       good++;
        
                    } 
                    if (eElement.getElementsByTagName("alignment").item(0).getTextContent().equals("Bad")) {
                        gravar_herois_bad.printf(eElement.getAttribute("id") + ", " + eElement.getElementsByTagName("name").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("popularity").item(0).getTextContent() + ", " + eElement.getElementsByTagName("alignment").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("gender").item(0).getTextContent() + ", " + eElement.getElementsByTagName("height_m").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("weight_kg").item(0).getTextContent() + ", " + eElement.getElementsByTagName("hometown").item(0).getTextContent() + ", "
                            + eElement.getElementsByTagName("intelligence").item(0).getTextContent() + ", " + eElement.getElementsByTagName("strength").item(0).getTextContent() + ", " + 
                            eElement.getElementsByTagName("durability").item(0).getTextContent() + ", " + eElement.getElementsByTagName("energy_Projection").item(0).getTextContent() + ", " 
                            + eElement.getElementsByTagName("fighting_Skills").item(0).getTextContent() + "\n");
                            bad ++;
                    }
                    if (eElement.getElementsByTagName("name").item(0).getTextContent().equals("Hulk")) {

                        //pesoHulk = Float.parseFloat(eElement.getAttribute("weight_kg"));
                        //altura = Float.parseFloat(eElement.getAttribute("height_m"));
                    }
                }
            }


         System.out.println("\nHerois  Bons:" + good + "/12");                               
        System.out.println("\nHerois  Maus:" + bad + "/12");
        /*System.out.println("\nMedia de Peso :" + peso/12);

        System.out.println("\nIMC HULK :" + pesoHulk/(altura*altura));*/



         herois.close();
         herois_good.close();
         herois_bad.close();
        } catch (Exception e) {
            System.out.println("Fail");
            e.printStackTrace();
        }
    }
}
