CREATE TABLE public.usuario (
	login varchar(100) NOT NULL,
	nome_clo varchar(100) NOT NULL,
	cidade_natal varchar(100) NOT NULL,
	PRIMARY KEY (login)
);


CREATE TABLE public.bloqueados (
	p1 varchar(100) NOT NULL,
	p2 varchar(100) NOT NULL,
	motivos varchar(100) NOT NULL,
  b1 varchar(100) NOT NULL,
	PRIMARY KEY (p1, p2, motivos)
);


CREATE TABLE public.conhece (
	c01 varchar(100) NOT NULL,
	co2 varchar(100) NOT NULL,
	PRIMARY KEY (c01, co2)
);


CREATE TABLE public.gosta (
	login_g varchar(100) NOT NULL,
	rating integer NOT NULL,
	uri varchar(100) NOT NULL,
	PRIMARY KEY (login_g)
);


CREATE TABLE public.artista_musical (
	id integer NOT NULL,
	nome_artista varchar(100) NOT NULL,
	pais varchar(100) NOT NULL,
	genero varchar(100) NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE public.cantor (
	c1 integer NOT NULL,
	c2 integer NOT NULL,
	PRIMARY KEY (c1, c2)
);


CREATE TABLE public.banda (
	b1 integer NOT NULL,
	PRIMARY KEY (b1)
);


CREATE TABLE public.musico (
	id integer NOT NULL,
	nome varchar(100) NOT NULL,
	estilo varchar(100) NOT NULL,
	d_nasc date NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE public.categoria (
	nome_cat varchar(100) NOT NULL,
	PRIMARY KEY (nome_cat)
);




CREATE TABLE public.subcategoria (
	s1 varchar(100) NOT NULL,
	PRIMARY KEY (s1)
);


CREATE TABLE public.filme (
	nome varchar(100) NOT NULL,
	id integer NOT NULL,
	dt_lan date NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE public.diretor_ (
	id integer NOT NULL,
	tel varchar(100) NOT NULL,
	endereco varchar(100) NOT NULL
);


CREATE TABLE public.ator_ (
	id integer NOT NULL,
	tel varchar(100) NOT NULL,
	endereco varchar(100) NOT NULL
);


CREATE TABLE public.gostaf (
	login_gf varchar(100) NOT NULL,
	rating integer NOT NULL,
	uri varchar(100) NOT NULL,
	PRIMARY KEY (login_gf)
);


CREATE TABLE public.bandapossuimusico (
	idb integer NOT NULL,
	idm integer NOT NULL
);

CREATE INDEX index_idb ON public.bandapossuimusico
	(idb);
CREATE INDEX index_idm ON public.bandapossuimusico
	(idm);


ALTER TABLE public.bloqueados ADD CONSTRAINT FK_bloqueados__p1 FOREIGN KEY (p1) REFERENCES public.usuario(login);
ALTER TABLE public.bloqueados ADD CONSTRAINT FK_bloqueados__p2 FOREIGN KEY (p2) REFERENCES public.usuario(login);
ALTER TABLE public.conhece ADD CONSTRAINT FK_conhece__c01 FOREIGN KEY (c01) REFERENCES public.usuario(login);
ALTER TABLE public.conhece ADD CONSTRAINT FK_conhece__co2 FOREIGN KEY (co2) REFERENCES public.usuario(login);
ALTER TABLE public.gosta ADD CONSTRAINT FK_gosta__login_g FOREIGN KEY (login_g) REFERENCES public.usuario(login);
ALTER TABLE public.cantor ADD CONSTRAINT FK_cantor__c1 FOREIGN KEY (c1) REFERENCES public.artista_musical(id);
ALTER TABLE public.cantor ADD CONSTRAINT FK_cantor__c2 FOREIGN KEY (c2) REFERENCES public.artista_musical(id);
ALTER TABLE public.banda ADD CONSTRAINT FK_banda__b1 FOREIGN KEY (b1) REFERENCES public.artista_musical(id);
ALTER TABLE public.subcategoria ADD CONSTRAINT FK_subcategoria__s1 FOREIGN KEY (s1) REFERENCES public.categoria(nome_cat);
ALTER TABLE public.gostaf ADD CONSTRAINT FK_gostaf__login_gf FOREIGN KEY (login_gf) REFERENCES public.usuario(login);
ALTER TABLE public.bandapossuimusico ADD CONSTRAINT FK_bandapossuimusico__idb FOREIGN KEY (idb) REFERENCES public.banda(b1);
ALTER TABLE public.bandapossuimusico ADD CONSTRAINT FK_bandapossuimusico__idm FOREIGN KEY (idm) REFERENCES public.musico(id);
