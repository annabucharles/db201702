# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Modifique este aquivo README.md com as informações adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usuário GitLab para cada integrante do grupo.

- Katielen de Castro Silva,1507575, k3ati

- Ana Flávia Yanaze Muranobu, 1614673, anayanaze


## Descrição da aplicação a ser desenvolvida 

Descreva aqui uma visão geral da aplicação que será desenvolvida pelo grupo durante o semestre. **Este texto deverá ser escrito quando requisitado pelo professor.** O conteúdo vai evoluir à medida em que o grupo avança com a implementação.
